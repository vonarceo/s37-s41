const express = require('express')
const dotenv = require('dotenv')
const mongoose = require('mongoose')
const cors = require('cors')
const userRoutes = require('./routes/userRoutes')
const courseRoutes = require('./routes/courseRoutes')

dotenv.config()

const app = express()
const port = 8001

//MongoDB Connection

mongoose.connect(`mongodb+srv://vonarceo:${process.env.PASSWORD}@cluster0.x7hwzcj.mongodb.net/booking-system-api?retryWrites=true&w=majority`, {
	useNewUrlParser: true,
	useUnifiedTopology: true
})

let db = mongoose.connection
db.once('open', () => console.log('Connected to mongoDB!'))

//MongoDB Connection End
//to inittialize cors // to avoid cors error when trying to send request to our server

app.use(cors())
app.use(express.json())
app.use(express.urlencoded({extended: true}))

// routes
app.use('/users', userRoutes)
app.use('/courses', courseRoutes)
// routes end


app.listen(port, () => {
	console.log(`API is now running on localhost:${port}`)
})