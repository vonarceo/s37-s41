const express = require('express')
const router = express.Router()
const CourseController = require('../controllers/CourseController')
const auth = require('../auth')

// Create single course
router.post('/create', auth.verify, (request, response) => {
	const data = {
		course: request.body,
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

	CourseController.addCourse(data).then((result) => {
		response.send(result)
	})
})


// create all course

router.get('/', (request, response) => {
	CourseController.getAllCourses().then((result) => {
		response.send(result)
	})
})

// get all active course

router.get('/active', (request, response) => {
	CourseController.getAllActive().then((result) => {
		response.send(result)
	})
})

router.get('/:courseId', (request, response) => {
	CourseController.getCourse(request.params.courseId).then((result) => {
		response.send(result)
	})
})


// update single course

router.patch('/:courseId/update', auth.verify, (request, response) => {
	CourseController.updateCourse(request.params.courseId).then((result) => {
		response.send(result)
	})
})


router.patch('/:courseId/archive', auth.verify, (request, response) => {
	CourseController.archiveCourse(request.params.courseId, request.body).then((result) => {
		response.send(result)
	})
})


module.exports = router 
